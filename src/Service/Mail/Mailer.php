<?php

namespace Adrosoftware\Core\Service\Mail;

class Mailer
{
    /**
     * @var MailerStrategyInterface $mailer
     */
    protected $mailer;

    /**
     * @var Array $config
     */
    protected $config;
    /**
     * @var Array $data
     */
    protected $data = []; 

    public function __construct(array $config)
    {
        $this->config = $config;
        $name = $this->getMailerClass();
        if (true === (bool) $this->config['test']['active']) {
            $mailer = new $name($config['providers'][$this->config['test']['transport']]);
        } else {
            $mailer = new $name($config['providers'][$this->config['default']]);
        }
        $this->setMailer($mailer);
    }

    public function setTo($to)
    {
        $this->data['to'] = $to;
        return $this;
    }
    
    public function setBody($body)
    {
        $this->data['html'] = $body;
        return $this;
    }

    public function setText($text)
    {
        $this->data['text'] = $text;
        return $this;
    }
    public function setSubject($subject)
    {
        $this->data['subject'] = $subject;
        return $this;
    }

    public function send()
    {
        $this->mailer->prepareMail($this->data)->send();
    }

    protected function setMailer($mailer)
    {
        $this->mailer = $mailer;
    }

    protected function getMailer()
    {
        $this->mailer;
    }

    protected function getMailerClass()
    {
        if (true === (bool) $this->config['test']['active']) {
            return __NAMESPACE__.'\Mailer'.'\\'.ucfirst($this->config['test']['transport']);
        }
        return __NAMESPACE__.'\Mailer'.'\\'.ucfirst($this->config['default']);
    }
}
