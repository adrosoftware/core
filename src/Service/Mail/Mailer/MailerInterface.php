<?php

namespace Adrosoftware\Core\Service\Mail\Mailer;

interface MailerInterface
{
    /**
     * Send email
     */
    public function send();
    /**
     * Prepare mail data
     */
    public function prepareMail(array $data);
}