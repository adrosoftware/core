<?php

namespace Adrosoftware\Core\Service\Mail\Mailer;

use SendGrid\Email;
use SendGrid\Content;
use SendGrid\Mail;
use \SendGrid as ParentSendgrid;

class Sendgrid implements MailerInterface
{
    protected $sendgrid;
    protected $message;
    protected $config;

    public function __construct(array $config)
    {
        $this->config = $config;
        $this->sendgrid = new ParentSendgrid($this->config['api_key']);
    }

    public function send()
    {
        return $this->sendgrid->client->mail()->send()->post($this->message);
    }

    public function prepareMail(array $data)
    {
        $email = array_keys($this->config['from']);
        $name = array_values($this->config['from']);
        $this->message = new Mail(
            new Email($name[0], $email[0]), 
            $data['subject'], 
            new Email('', $data['to']),
            new Content("text/html", $data['html'])
        );
        return $this;
    }
}
