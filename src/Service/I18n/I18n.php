<?php

namespace Adrosoftware\Core\Service\I18n;

class I18n
{
    public static function t($id, array $parameters = array(), $domain = null, $locale = null)
    {
        return app('translator')->trans($id, $parameters, $domain, $locale);
    }

    public static function tl($id, $locale = null, array $parameters = array(), $domain = null)
    {
        return app('translator')->trans($id, $parameters, $domain, $locale);
    }
}
