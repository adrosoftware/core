<?php

namespace Adrosoftware\Core\Service\Hash;

use RuntimeException;

class Hash
{
    /**
     * Default crypt cost factor.
     *
     * @var int
     */
    protected static $rounds = 10;

    /**
     * Hash the given value.
     *
     * @param  string  $value
     * @param  array   $options
     * @return string
     *
     * @throws \RuntimeException
     */
    public static function make($value, array $options = [])
    {
        $cost = isset($options['rounds']) ? $options['rounds'] : self::$rounds;
        $hash = password_hash($value, PASSWORD_BCRYPT, ['cost' => $cost]);

        if ($hash === false) {
            throw new RuntimeException('Bcrypt hashing not supported.');
        }

        return $hash;
    }

    /**
     * Check the given plain value against a hash.
     *
     * @param  string  $value
     * @param  string  $hashedValue
     *
     * @return bool
     */
    public static function check($value, $hashedValue)
    {
        if (strlen($hashedValue) === 0) {
            return false;
        }

        return password_verify($value, $hashedValue);
    }

    /**
     * Check if the given hash has been hashed using the given options.
     *
     * @param  string  $hashedValue
     * @param  array   $options
     * @return bool
     */
    public static function needsRehash($hashedValue, array $options = [])
    {
        $cost = isset($options['rounds']) ? $options['rounds'] : self::$rounds;
        return password_needs_rehash($hashedValue, PASSWORD_BCRYPT, ['cost' => $cost]);
    }

    /**
     * Re-hash the value if it needed.
     *
     * @param $string
     * @param array $options
     * @return string
     */
    public static function rehashIfNecessary($string, array $options = [])
    {
        return self::needsRehash($string, $options) ? self::make($string, $options) : $string;
    }
}
