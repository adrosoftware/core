<?php

namespace Adrosoftware\Core\Container;

class Container extends \DI\Container implements \Interop\Container\ContainerInterface
{
    /**
     * Magic method to get a container element
     *
     * @param string $name The name od the element to get
     * @return mixed
     */
    public function __get($name)
    {
        return $this->get($name);
    }

    /**
     * Magic method isset
     *
     * @param string $name The name of the element to be checked
     * @return bool
     */
    public function __isset($name)
    {
        return $this->has($name);
    }
}
