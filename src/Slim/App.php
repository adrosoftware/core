<?php

namespace Adrosoftware\Core\Slim;

use Adrosoftware\Core\Container\Container;
use DI\ContainerBuilder;

/**
 * Slim application configured with PHP-DI.
 *
 * As you can see, this class is very basic and is only useful to get started quickly.
 * You can also very well *not* use it and build the container manually.
 */
class App extends \Slim\App
{
    public function __construct()
    {
        $definitions = require ROOT_PATH . '/config/slim.php';
        $builder = new ContainerBuilder(Container::class);
        $builder->addDefinitions($definitions);
        $this->configureContainer($builder);
        $container = $builder->build();

        parent::__construct($container);
    }

    /**
     * Override this method to configure the container builder.
     *
     * For example, to load additional configuration files:
     *
     *     protected function configureContainer(ContainerBuilder $builder)
     *     {
     *         $builder->addDefinitions(__DIR__ . 'my-config-file.php');
     *     }
     */
    protected function configureContainer(ContainerBuilder $builder)
    {
    }
}
