<?php

namespace Adrosoftware\Core\Provider;

use Adrosoftware\Core\Provider\ServiceProviderInterface;
use DI;
use Interop\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\AutoExpireFlashBag;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\MemcacheSessionHandler;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\NativeSessionHandler;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class SessionProvider implements ServiceProviderInterface 
{

    public function register( ContainerInterface $container )
    {
        $container->set('session', function($container){
            return new Session($container->get('session.storage'), null, new AutoExpireFlashBag('_axiuayan_pms_flashes'));
        });

        $container->set('session.storage', function($container){
            return new NativeSessionStorage(
                $container->get('app.settings')['session'],
                $container->get('session.storage.handler')
            );
        });

        $container->set('session.storage.handler',function($container){
            return new NativeSessionHandler();
        });

        $container->set(SessionInterface::class, DI\get('session'));
    }

    public function boot(ContainerInterface $container)
    {
    }
}
