<?php

namespace Adrosoftware\Core\Provider;

use Adrosoftware\Core\Provider\ServiceProviderInterface;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container as IlluminateContainer;
use Interop\Container\ContainerInterface;

class DatabaseProvider implements ServiceProviderInterface
{
    public function register(ContainerInterface $container)
    {
        $capsule = new Capsule;
        $config = $container->get('app.settings')['database'];
        $capsule->addConnection($config['connections']['write'], 'default');
        $capsule->setEventDispatcher(new Dispatcher(new IlluminateContainer));
        $capsule->setAsGlobal();
        $capsule->bootEloquent();
        $container->set('db', function ($container) use ($capsule){
            return $capsule->getDatabaseManager();
        });
    }

    public function boot(ContainerInterface $container)
    {
    }
}