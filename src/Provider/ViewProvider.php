<?php

namespace Adrosoftware\Core\Provider;

use Adrosoftware\Core\Provider\ServiceProviderInterface;
use Adrosoftware\Core\Service\View\View;
use League\Plates\Engine;
use League\Plates\Extension\Asset;
use League\Plates\Extension\URI;
use Interop\Container\ContainerInterface;

class ViewProvider implements ServiceProviderInterface
{
    public function register(ContainerInterface $container)
    {
        $request = $container->get('request');
        $engine = new Engine(ROOT_PATH . '/resources/views', 'phtml');
        $engine->loadExtension(new Asset(ROOT_PATH . '/public',true));
        $engine->loadExtension(new URI($request->getRequestTarget()));

        $container->set('view', function ($container) use ($engine) {
            return new View($engine);
        });
    }

    public function boot(ContainerInterface $container)
    {
    }
}