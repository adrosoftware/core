<?php

namespace Adrosoftware\Core\Provider;

use Interop\Container\ContainerInterface;

interface ServiceProviderInterface
{
    public function register(ContainerInterface $pimple);

    public function boot(ContainerInterface $pimple);
}
