<?php

namespace Adrosoftware\Core\Provider;

use Adrosoftware\Core\Provider\ServiceProviderInterface;
use Adrosoftware\Core\Service\Mail\Mailer;
use Interop\Container\ContainerInterface;

class MailerProvider implements ServiceProviderInterface
{
    public function register(ContainerInterface $container)
    {
        $config = $container->get('app.settings')['mail'];
        $container->set('mailer', function ($container) use ($config) {
            return new Mailer($config);
        });
    }

    public function boot(ContainerInterface $container)
    {
    }
}