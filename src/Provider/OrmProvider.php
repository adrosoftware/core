<?php

namespace Adrosoftware\Core\Provider;

use Adrosoftware\Core\Provider\ServiceProviderInterface;
use Interop\Container\ContainerInterface;
use Enphpity\Orm\Enphpity;

class OrmProvider implements ServiceProviderInterface
{
    public function register(ContainerInterface $container)
    {
        $config = $container->get('app.settings')['database'];

        foreach ($config['connections'] as $name => $connectionParams) {
            $config['connections'][$name]['driver'] = 'pdo_' . $connectionParams['driver'];
            $config['connections'][$name]['dbname'] = $connectionParams['database'];
        }
        $enphpity = new Enphpity($config);
        $container->set('orm', function($container) use ($enphpity) {
            return $enphpity->orm();
        });
    }

    public function boot(ContainerInterface $container)
    {
    }
}