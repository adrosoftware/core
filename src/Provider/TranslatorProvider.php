<?php

namespace Adrosoftware\Core\Provider;

use Adrosoftware\Core\Provider\ServiceProviderInterface;
use Interop\Container\ContainerInterface;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\MessageSelector;
use Symfony\Component\Translation\Loader\ArrayLoader;

class TranslatorProvider implements ServiceProviderInterface
{
    public function register(ContainerInterface $container)
    {
        if (!isset($container->translator)) {

            $container->set('translator', function ($container) {
                $translator = new Translator('es_MX', new MessageSelector());
                $translator->addLoader('array', new ArrayLoader());
                $translator->setFallbackLocales(array('es'));

                return $translator;
            });
        }
    }

    public function boot(ContainerInterface $container)
    {
        class_alias(\Adrosoftware\Core\Service\I18n\I18n::class, 'I18n');

        $settings = $container->get('app.settings')['i18n'];

        $path = $settings['path'];

        //es 
        $es = require $path.'/es/messages.php';
        $container->translator->addResource('array', $es , 'es');

        //en
        $en = require $path.'/en/messages.php';
        $container->translator->addResource('array', $en , 'en');
    }
}