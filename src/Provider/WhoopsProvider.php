<?php

namespace Adrosoftware\Core\Provider;

use Adrosoftware\Core\Provider\ServiceProviderInterface;
use Adrosoftware\Core\Error\Handler\WhoopsErrorHandler;
use Interop\Container\ContainerInterface;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Handler\JsonResponseHandler;
use Whoops\Util\Misc;
use Whoops\Run;

class WhoopsProvider implements ServiceProviderInterface
{
    public function register(ContainerInterface $container)
    {
        $env = env('ENV','production');
        if ('development' === $env) {
            $run     = new Run;
            $handler = new PrettyPageHandler;

            // Set the title of the error page:
            $handler->setPageTitle("Whoops! There was a problem.");

            $run->pushHandler($handler);

            // Add a special handler to deal with AJAX requests with an
            // equally-informative JSON response. Since this handler is
            // first in the stack, it will be executed before the error
            // page handler, and will have a chance to decide if anything
            // needs to be done.
            if (Misc::isAjaxRequest()) {
              $run->pushHandler(new JsonResponseHandler);
            }

            // Register the handler with PHP, and you're set!
            $run->register();

            $container->set('whoops', function($c) use ($run){
                return $run;
            });
            $container->set('errorHandler', function ($c) {
                return new WhoopsErrorHandler($c);
            });
            $container->set('phpErrorHandler', function ($c) {
                return new WhoopsErrorHandler($c);
            });
        }
    }

    public function boot(ContainerInterface $container)
    {
    }
}