<?php

if (! function_exists('env')) {
    /**
     * Gets the value of an environment variable. Supports boolean, empty and null.
     *
     * @param  string  $key
     * @param  mixed   $default
     * @return mixed
     */
    function env($key, $default = null)
    {
        $value = getenv($key);
        if ($value === false) {
            return value($default);
        }
        switch (strtolower($value)) {
            case 'true':
            case '(true)':
                return true;
            case 'false':
            case '(false)':
                return false;
            case 'empty':
            case '(empty)':
                return '';
            case 'null':
            case '(null)':
                return;
        }
        return $value;
    }
}

if (! function_exists('value')) {
    /**
     * Return the default value of the given value.
     *
     * @param  mixed  $value
     * @return mixed
     */
    function value($value)
    {
        return $value instanceof Closure ? $value() : $value;
    }
}

function app($service = null)
{
    $app = \Adrosoftware\Core\Application::getInstance();

    if ($service) {
        return $app->get($service);
    }

    return $app;
}

if (!function_exists('guid')) {
    function guid()
    {
        $guid = '';
        if (function_exists('com_create_guid') === true) {
            $guid = com_create_guid();
            return $guid;
        }

        $guid = sprintf(
            '%04X%04X-%04X-%04X-%04X-%04X%04X%04X',
            mt_rand(0, 65535),
            mt_rand(0, 65535),
            mt_rand(0, 65535),
            mt_rand(16384, 20479),
            mt_rand(32768, 49151),
            mt_rand(0, 65535),
            mt_rand(0, 65535),
            mt_rand(0, 65535)
        );

        return $guid;
    }
}

/**
 * Returns an instance of the container registered in the application
 * 
 * @return \Interop\Container\ContainerInterface
 */
function container()
{
    $app = app();
    return $app->getContainer();
}
/**
 * Creates an instance of a class using the dependency injection container
 * 
 * @return mixed
 */
function make($class)
{
    return container()->make($class);
}

use Illuminate\Support\Debug\Dumper;

if (! function_exists('dd')) {
    /**
     * Dump the passed variables and end the script.
     *
     * @param  mixed
     * @return void
     */
    function dd()
    {
        d(func_get_args());
        die(1);
    }
}

if (! function_exists('d')) {
    /**
     * Dump the passed variables and end the script.
     *
     * @param  mixed
     * @return void
     */
    function d()
    {
        array_map(function ($x) {
            (new \Illuminate\Support\Debug\Dumper)->dump($x);
        }, func_get_args());
    }
}
