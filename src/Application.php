<?php

namespace Adrosoftware\Core;

use Dotenv\Dotenv;
use Adrosoftware\Core\Slim\App as SlimApp;
use Adrosoftware\Core\Provider\ServiceProviderInterface;

class Application
{
    protected static $instance;

    protected $app;
    
    protected $config;

    protected $container;

    protected $providers = [];

    protected $booted = false;

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function __construct($rootPath = null)
    {
        self::$instance = $this;
        $this->path = $rootPath;
        $this->createConstants();
        $this->loadEnvironmentVariables();
        $this->setConfig();
        $this->app = $this->createApp();
        $this->bootstrap();
        $this->init();
    }

    public function get($service, $default = null)
    {
        if ($this->container->has($service)) {
            return $this->container->get($service);
        }

        return $default;
    }

    public function getContainer()
    {
        return $this->container;
    }

    protected function init()
    {
    }
    
    protected function createConstants()
    {
        define('ROOT_PATH', $this->path);
        define('PUBLIC_PATH', $this->path . '/public/');
        define('VIEWS_PATH', $this->path . '/resources/views/');
        define('LANG_PATH', $this->path . '/resources/lang/');
        define('CACHE_PATH', $this->path . '/storage/cache');
    }

    protected function bootstrap()
    {
        $this->bindConfigurationToContainer();
    }

    protected function loadEnvironmentVariables()
    {
        $dotenv = new Dotenv(ROOT_PATH);
        $dotenv->overload();
    }

    protected function setConfig()
    {
        foreach (glob(ROOT_PATH.'/config/*.php') as $configFile) {
            if ($configFile !== ROOT_PATH.'/config/slim.php') {
                require $configFile;
            }
        }
        $appConfig = [];
        foreach ($config as $key => $value) {
            $appConfig[$key] = $value;
        }
        $this->configs = $appConfig;
        return $this;
    }

    /**
     * Run application.
     * 
     * @return Response
     */
    public function run()
    {
        if ($this->container->has('session')) {
            $this->container->get('session')->start();
        }
        if(!$this->booted){
            $this->boot();
        }
        return $this->app->run();
    }
    /**
     * Registers a service provider.
     *
     * @param ServiceProviderInterface $provider A ServiceProviderInterface instance
     *
     * @return Application
     */
    public function register(ServiceProviderInterface $provider, array $values = array())
    {
        $provider->register($this->container);
        $this->providers[] = $provider;
        return $this;
    }


    /*
    |---------------------------------------------------
    | Protected
    |---------------------------------------------------
    */

    /**
     * Boots all service providers.
     *
     * This method is automatically called by run(), but you can use it
     * to boot all service providers when not handling a request.
     */
    protected function boot()
    {
        if (!$this->booted) {
            foreach ($this->providers as $provider) {
                $provider->boot($this->container);
            }
            $this->booted = true;
        }
    }

    protected function createApp()
    {
        $app = new SlimApp();

        $this->container = $app->getContainer();

        $this->container->set('slim', function ($container) use ($app) {
            return $app;
        });

        return $app;
    }

    protected function bindConfigurationToContainer()
    {
        $appConfig = $this->configs;
        foreach ($appConfig as $key => $value) {
            $appConfig[$key] = $value;
        }
        $this->container->set('app.settings', function ($container) use ($appConfig) {
            return $appConfig;
        });
    }


}