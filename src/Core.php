<?php

namespace Adrosoftware\Core;

class Core
{
    public static $version = '0.1.1';

    public static function version()
    {
        return self::$version;
    }
}
