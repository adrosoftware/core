<?php

namespace Adrosoftware\Core\Error\Handler;

use Throwable;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Whoops\Run as WhoopsRun;

class WhoopsErrorHandler
{
    public function __construct($container)
    {
        $this->container = $container;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, Throwable $exception)
    {
        $container      = $this->container;
        $settings       = $container->get('settings');
        $environment    = $container->get('environment');
        foreach ($container->get('whoops')->getHandlers() as $handler) {
            if($handler instanceof \Whoops\Handler\PrettyPageHandler){
                $prettyPageHandler = $handler;
                continue;
            }
        }

        // Add more information to the PrettyPageHandler
        $prettyPageHandler->addDataTable('Slim Application', [
            'Script Name'       => $environment->get('SCRIPT_NAME'),
            'Request URI'       => $environment->get('PATH_INFO') ?: '<none>',
        ]);
        $prettyPageHandler->addDataTable('Slim Application (Request)', array(
            'Accept Charset'  => $request->getHeader('ACCEPT_CHARSET') ?: '<none>',
            'Content Charset' => $request->getContentCharset() ?: '<none>',
            'Path'            => $request->getUri()->getPath(),
            'Query String'    => $request->getUri()->getQuery() ?: '<none>',
            'HTTP Method'     => $request->getMethod(),
            'Base URL'        => (string) $request->getUri(),
            'Scheme'          => $request->getUri()->getScheme(),
            'Port'            => $request->getUri()->getPort(),
            'Host'            => $request->getUri()->getHost(),
        ));
        $handler = WhoopsRun::EXCEPTION_HANDLER;
        ob_start();
        $container->get('whoops')->$handler($exception);
        $content = ob_get_clean();
        $code    = $exception instanceof HttpException ? $exception->getStatusCode() : 500;
        return $response
                ->withStatus($code)
                ->withHeader('Content-type', 'text/html')
                ->write($content);
    }
}
